const express = require('express')
const router = express.Router();
const db = require("../../DB/db");

const nameOfCollection = 'Team';

//Get Team
router.get('/',(req,res,next) => {
   
})

//by Id
router.post('/',(req,res,next)=>{
     let DOC = db.collection(nameOfCollection)
     DOC.get().then((snapshot) => { 
      let Data = [];
      let isMember = false;
      snapshot.forEach((doc) => {

        if(doc.data().admin === req.body.id) {
        Data.push({ id: doc.id, data: doc.data() }); }

            DOC.doc(doc.id).collection("members").get().then((snapshot) => { 
              isMember=true
              snapshot.forEach((data) => { 
                data.data().googleId === req.body.id ?       
                Data.push({ memberid: data.id, data: doc.data() }) : "";
                isMember ? res.send(Data) : "" ;
              })
            })
      });
     // isMember ? "" : res.send(Data) ;
     })
})

//create Team
router.post('/createTeam',(req,res,next) => {
    db.collection(nameOfCollection).add({
        name: req.body.Name,
        admin: req.body.Admin,
        adminname: req.body.AdminName,
        public: req.body.Public
      }).then(ref => {
        console.log('Added document with ID: ', ref.id);
        res.json({"err":"false","msg":"Data Store","teamid":ref.id})
      });
})

//Join Team
router.post('/joinTeam',(req,res,next) => {
    let doc = db.collection(nameOfCollection).doc(req.body.teamid);
    // console.log(req.body.member)
    doc.get().then((snapshot) => {
        if (snapshot.exists) {
            console.clear()
            doc.collection("members").add(req.body.member).then(ref => {
                console.log('Added document with ID: ', ref.id);
                res.json({"err":"false","msg":"Data Store","memberid":ref.id})
              });
            res.send({"exists":"true","Team":snapshot.data()})
        }else{
            res.send({"exists":"false"})
        }
    })
})

//Delete Data
router.post('/deleteTeam',(req,res,next) => {
    
    res.json({"err":"false","msg":"Data Store"})
})


module.exports = router
