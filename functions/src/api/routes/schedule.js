const express = require('express')
const router = express.Router();

const DB = require("../../DB/dbFunction")
const Schedule = require("../../Model/ScheduleModel")
const nameOfCollection = 'ScheduleTask';

//Get Data
router.get('/',(req,res) => {
    DB.getData(nameOfCollection,res);
})

router.post('/',(req,res)=>{
    DB.getData_ByID(nameOfCollection,res,req.body.id);
})

//Add Data
router.post('/addTask',(req,res) => {
    Schedule.ScheduleModel(req.body)
    res.json({"err":"false","msg":"Data Store"})
})

//Delete Data
router.delete('/deleteTask',(req,res) => {
    DB.deleteData(nameOfCollection,req.body.id);
    res.json({"err":"false","msg":"Data Store"})
})

router.post('/deleteTask',(req,res) => {
    DB.deleteData(nameOfCollection,req.body.id);
    res.json({"err":"false","msg":"Data Store"})
})

module.exports = router
