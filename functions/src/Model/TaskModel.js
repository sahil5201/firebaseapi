const DB = require("../DB/db");
const dbFunction = require("../DB/dbFunction");

class TaskModel {
    TaskModel(BodyData) {
        let Data =
            {
                Task: BodyData.Task,
                userId: BodyData.userID,
                Date: Date.now()
            }
        dbFunction.addData("Task", Data);
    }
}

module.exports = new TaskModel();
